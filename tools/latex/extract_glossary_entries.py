#!/usr/bin/python3

grammar = {}

def format_grammar(acronym):
    global grammar
    if acronym in grammar:
        return grammar[acronym]['shorthand']
    else:
        return "XXX"

def populate_grammar(c):
    global grammar
    for row in c.execute("SELECT * FROM `grammar` ORDER BY label"):
        grammar[row['label']] = {
                'shorthand': row['shorthand'],
                'gloss': row['gloss'],
                }


import sqlite3

conn = sqlite3.connect("glossary.db")
conn.row_factory = sqlite3.Row
c = conn.cursor()

populate_grammar(c)

print("%%% PRIMARY %%%\n")

for row in c.execute("SELECT * FROM `glossary-primary` ORDER BY label"):
    if row['gloss'] is None or row['ipa'] is None or row['pos'] is None or row['grammatical'] is None:
        continue
    entry = "\\newglossaryentry{" + row['label'] + "}{" +\
            "name={" + row['name'] + "}, " +\
            "description={"
    if row['oed'] is not None: entry += "\\textsuperscript{\\textcolor{darkgray}{\small[" + row['oed'] + "]}} "
    entry += "\\ipa{\\footnotesize\\textcolor{gray}{/" + row['ipa'] + "/}} "
    entry += "\\Heb{\\footnotesize\\textcolor{gray}{" + format_grammar(row['pos']) + "}} "
    entry += "\hfill $~$ "
    entry += "\Heb{" + row['gloss'] + "}"
    entry += "}" +\
            "}"
    #if row['players'] is not None:
    print(entry)

print("\n%%% SECONDARY %%%\n")

for row in c.execute("SELECT * FROM `glossary-secondary` ORDER BY label"):
    entry = "\\newglossaryentry{" + row['label'] + "}{"
    if row['subname'] is not None:
        entry += "name={" + row['name'] + " (" + row['subname']+ ")}, "
    else:
        entry += "name={" + row['name'] + "}, "
    entry += "parent={" + row['parent'] + "}, " +\
            "description={"
    entry += "\\ipa{\\textcolor{gray}{\\footnotesize /" + row['ipa'] + "/}} "
    if row['form'] is not None: entry += "\\Heb{\\footnotesize\\textcolor{gray}{" + format_grammar(row['form']) + "}}"
    entry += "\hfill $~$ "
    if row['gloss'] is not None:
        entry += "\\Heb{" + row['gloss'] + "}"
    entry += "}"
    entry += "}"
    print(entry)
    if row['crossref'] == 1:
        entry = "\\newglossaryentry{" + row['label'] + "-crossref}{" +\
                "name={" + row['name'] + "}, " +\
                "description={"
        entry += "→ \\glossentryname{" + row['parent']+ "}"
        entry += "\hfill~"
        entry += "}"
        entry += "}"
        print(entry)
        print("\\glsadd{" + row['label'] + "-crossref}")
