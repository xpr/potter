#!/usr/bin/python3

import argparse
import dominate
from dominate import tags
import os
import re
import yaml
import pdb

primary = []
secondary = []
grammar = []

def read_glossary(glossdir):
    global primary, secondary, grammar
    primary = list(yaml.load_all(open(os.path.join(glossdir, 'primary.yaml'))))[0]
    secondary = list(yaml.load_all(open(os.path.join(glossdir, 'secondary.yaml'))))[0]
    grammar = list(yaml.load_all(open(os.path.join(glossdir, 'grammar.yaml'))))[0]

def html_head():
    tags.meta(charset='utf-8')
    #tags.link(rel="stylesheet", type="text/css", href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css")
    #tags.link(rel="stylesheet", type="text/css", href="https://cdn.jsdelivr.net/flexboxgrid/6.2.0/flexboxgrid.min.css")
    tags.script(type="text/javascript", src="https://code.jquery.com/jquery-latest.js")
    tags.link(rel="stylesheet", type="text/css", href="static/css/style.css")
    playscript = """
function onlyPlayOneIn(container) {
	container.addEventListener("play", function(event) {
		audio_elements = container.getElementsByTagName("audio")
		for(i=0; i < audio_elements.length; i++) {
			audio_element = audio_elements[i];
			if (audio_element !== event.target) {
				audio_element.pause();
				audio_element.currentTime = 0;
			}
		}
	}, true);
}
document.addEventListener("DOMContentLoaded", function() {
	onlyPlayOneIn(document.body);
});
    """
    tags.script(dominate.util.raw(playscript))

def output_text(left_raw, right_raw):
    def primary_name(match):
        lemma = next((l for l in primary if l['label'] == match.group(1)), None)
        if lemma and lemma['name']:
            return '<a href="#' + match.group(1) + '">' + lemma['name'] + '</a>'
        else:
            sublemma = next((l for l in secondary if l['label'] == match.group(1)), None)
            if sublemma and sublemma['name']:
                return '<a href="#' + sublemma['parent'] + '">' + sublemma['name'] + '</a>'
            else:
                return match.group(1)
    def link_parent(match):
        sublemma = next((l for l in secondary if l['label'] == match.group(2)), None)
        if sublemma:
            return '<a href="#' + sublemma['parent'] + '">' + match.group(1) + '</a>'
        else:
            return '<a href="#' + match.group(2) + '">' + match.group(1) + '</a>'
    def insert_audio(match):
        stripped_text = re.sub('<[^<]+?>', '', match.group(2))
        return match.group(1) + f'<a onclick="this.firstChild.play()" class="play_button"><audio src="static/audio/{stripped_text}.mp3"></audio>▸</a> ' + match.group(2)
    def prepare_left(raw):
        output = []
        for paragraph in raw:
            if len(paragraph) > 1:
                paragraph = re.sub(r'\{([^}]*)\}', primary_name, paragraph)
                paragraph = re.sub(r'\[([^]]*)\]\[([^]]*)\]', link_parent, paragraph)
                paragraph = re.sub(r'(^| )([^.]*(\.|$))', insert_audio, paragraph)
                output.append(paragraph)
        return output
    def prepare_right(raw):
        output = []
        for paragraph in raw:
            if len(paragraph) > 1:
                output.append(paragraph)
        return output
    left = prepare_left(left_raw)
    right = prepare_right(right_raw)
    with tags.table(cls='bilingual_text') as t:
        for paragraph_no in range(len(left)):
            with tags.tr():
                tags.td(dominate.util.raw(left[paragraph_no]), cls='leftside')
                if paragraph_no < len(right):
                    tags.td(dominate.util.raw(right[paragraph_no]), lang='hebrew', cls='rightside')

def output_glossary():
    global primary, secondary, grammar
    for lemma in sorted(primary, key=lambda k: k['name'].lower()):
        with tags.div(id=lemma['label']):
            tags.span(lemma['name'], cls='headword')
            if 'oed' in lemma.keys() and lemma['oed']:
                tags.span("[" + lemma['oed'] + "]", cls='oed')
            if 'ipa' in lemma.keys() and lemma['ipa']:
                tags.span("/" + lemma['ipa'] + "/", cls='ipa')
            if 'audio' in lemma.keys():
                if lemma['audio']:
                    dominate.util.raw('\n<a onclick="this.firstChild.play()" class="play_button"><audio preload="none" src="http://audio.oxforddictionaries.com/en/mp3/' + lemma['audio'] + '.mp3"></audio>▸</a>\n')
            else:
                dominate.util.raw('\n<a onclick="this.firstChild.play()" class="play_button"><audio preload="none" src="http://audio.oxforddictionaries.com/en/mp3/' + lemma['name'] + '_gb_1.mp3"></audio>▸</a>\n')
            if lemma['pos']:
                pos = next((g for g in grammar if g['label'] == lemma['pos']), None)
                if pos:
                    if pos['shorthand']:
                        tags.span(pos['shorthand'] + '‎', cls='grammar')
                else:
                    tags.span('!@#')
            if lemma['gloss']:
                tags.span('‏' + lemma['gloss'], lang='hebrew', cls='gloss')
            if 'note' in lemma.keys() and lemma['note']:
                tags.span('(' + lemma['note'] + ')', lang='hebrew', cls='note')
            sublemmas = [sublemma for sublemma in secondary if sublemma['parent'] == lemma['label']]
            if sublemmas:
                with tags.div(cls='sublemmas'):
                    for sublemma in sorted(sublemmas, key=lambda k: k['name'].lower()):
                        with tags.div():
                            tags.span(sublemma['name'], cls='subheadword', id=sublemma['label'])
                            if 'ipa' in sublemma.keys() and sublemma['ipa']:
                                tags.span("/" + sublemma['ipa'] + "/", cls='ipa')
                            if sublemma['form']:
                                form = next((g for g in grammar if g['label'] == sublemma['form']), None)
                                if form:
                                    tags.span(form['shorthand'] + '‎', cls='grammar')
                                else:
                                    tags.span('!@#')
                                if sublemma['gloss']:
                                    tags.span(sublemma['gloss'], lang='hebrew', cls='gloss')


def main():
    parser = argparse.ArgumentParser(
        description="⚙")
    parser.add_argument("-l", "--left", type=str,
                        help="left text file")
    parser.add_argument("-r", "--right", type=str,
                        help="right text file")
    parser.add_argument("-g", "--glossary", type=str,
                        help="glossary directory")
    #parser.add_argument("-l", "--lang", type=str, default=DEFAULT_LANG,
    #                    help="language")
    #parser.add_argument("-b", "--book", type=str, default=DEFAULT_BOOK,
    #                    help="book")
    args = parser.parse_args()

    read_glossary(args.glossary)

    doc = dominate.document(title='לקרוא באנגלית עם הארי פוטר')
    with doc.head:
        html_head()
    with doc.body:
        with tags.div(style="height: 45vh; overflow-y: scroll"):
            output_text(open(args.left), open(args.right))
        with tags.div(style="height: 45vh; overflow-y: scroll; margin-top: 1em"):
            output_glossary()
    print(dominate.util.unescape(str(doc)))


if __name__ == "__main__":
    main()
